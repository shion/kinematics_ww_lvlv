typedef TLorentzVector tlv;

///////////////////////////////////////////////////////
void solve_WW_lvlv(tlv Vlp, tlv Vlm, tlv Vmis,  //  input  (lp: l+, lm: l-)
		   tlv &Vv1, tlv &Vv2           // output  (solution 1 and 2 for the neutrino 4-vector)
		   ){

  const double mW=80.4;         // Always assuming a fixed W-mass

  //////// Aliases for the known momenta
  const float Emis = Vmis.E();
  const float mmis = Vmis.M();
  const TVector3 vmis = Vmis.Vect();
  const float mfrac = pow(mW/mmis,2);
  
  const float plp = Vlp.E();
  const TVector3 vlp = Vlp.Vect();
  
  const float plm = Vlm.E();
  const TVector3 vlm = Vlm.Vect();
     
  //////// Components in Eq. 12-13
  //  (12) nv * R = A_0
  //  (13) nv * S = B_0
  const TVector3 R = vlp - mfrac*vmis;
  const TVector3 S = vlm + ((mW*mW-2*plm*Emis+2*vlm*vmis)/(mmis*mmis)) * vmis;    
  const float  A_0 = plp - mfrac*Emis;
  const float  B_0 = plm + ((mW*mW-2*plm*Emis+2*vlm*vmis)/(mmis*mmis)) * Emis;
  
  //////// Components in Eq. 15
  const TVector3 r = R.Unit();    
  const TVector3 s = S.Unit();
  const float A = A_0/R.Mag();
  const float B = B_0/S.Mag();

  //////// Components in Eq. 17
  const float irs = r*s;
  const TVector3 ors = (r.Cross(s)).Unit();
  const float x = (A-B*irs)/(1-irs*irs);
  const float y = (-A*irs+B)/(1-irs*irs);

  // Direction of the neutrino
  TVector3 sol_nv_1 = x*r + y*s + sqrt(1-x*x-y*y-2*x*y*irs) * ors;
  TVector3 sol_nv_2 = x*r + y*s - sqrt(1-x*x-y*y-2*x*y*irs) * ors;
  
  // Momentum of the neutrino
  float sol_pv_1 = mmis*mmis/(2*(Emis-vmis*sol_nv_1));
  float sol_pv_2 = mmis*mmis/(2*(Emis-vmis*sol_nv_2));

  // Set up the 4-vector output
  Vv1.SetVectM(sol_pv_1*sol_nv_1, 0.);
  Vv2.SetVectM(sol_pv_2*sol_nv_2, 0.);

  return;
}

///////////////////////////////////////////////////////
void test(){

  // An example of real event
  tlv Vlp        (29.071702,-42.553021,-79.167679,94.463987);  // (px,py,pz,E)
  tlv Vlm        (-63.576691,9.926756,-113.708827,130.653103);
  tlv Vv_truth   (8.559310,27.226785,-133.144418,136.168997);
  tlv Vvbar_truth(20.734051,-3.154493,-93.117095,95.449699); 

  tlv Vmis = Vv_truth + Vvbar_truth;

  // Solve
  tlv Vv1, Vv2;  // always two solutions
  solve_WW_lvlv(Vlp, Vlm, Vmis, Vv1, Vv2);

  std::cout << "True neutrino momentum: "; Vv_truth.Print();
  std::cout << "Solution-1: ";             Vv1.Print();
  std::cout << "Solution-2: ";             Vv2.Print();

  return ;
}
